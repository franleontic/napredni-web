const express = require("express");
const app = express();
var path = require('path');
require('dotenv').config();

global.emails = []
global.latitudes = []
global.longitudes = []
global.datetimes = []

const { auth } = require('express-openid-connect');

const config = {
    authRequired: false,
    auth0Logout: true,
    secret: process.env.AUTH0_CLIENT_SECRET,
    baseURL: 'https://evil-catacombs-97626.herokuapp.com/',
    clientID: process.env.AUTH0_CLIENT_ID,
    issuerBaseURL: 'https://dev-lwegi59n.us.auth0.com'
  };

app.use(auth(config));
app.use(express.urlencoded());
app.use(express.json());

app.set("port", process.env.PORT || 3000)

var d = __dirname

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

var routes = require('./routes/home.routes');

app.use(routes)

app.use(express.static(path.join(d, 'public')));

app.use('/css', express.static(path.join(d, 'node_modules/bootstrap/dist/css')))
app.use('/js', express.static(path.join(d, 'node_modules/bootstrap/dist/js')))
app.use('/js', express.static(path.join(d, 'node_modules/jquery/dist')))

app.listen(app.get("port"), function(){
    console.log("Server started on port " + app.get("port"))
});
