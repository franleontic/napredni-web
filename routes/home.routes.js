
var express = require('express');
var router = express.Router();
const { requiresAuth } = require('express-openid-connect');


router.get("/", function(req, res){
    req.user = {
        isAuthenticated : req.oidc.isAuthenticated()
    };
    if (req.user.isAuthenticated)
        req.user.name = req.oidc.user.name;

    res.render("index", {user: req.user});
});

router.post("/locdata", (req, res) => {
    var email = req.body.email
    var latitude = req.body.latitude
    var longitude = req.body.longitude
    var dt = new Date().toLocaleString()

    if (global.emails.indexOf(email) === -1){
        if (global.emails.length == 5){
            global.emails.shift()
            global.latitudes.shift()
            global.longitudes.shift()
            global.datetimes.shift()
        }
    } else {
        var index = emails.indexOf(email)
        global.emails.splice(index, 1)
        global.latitudes.splice(index, 1)
        global.longitudes.splice(index, 1)
        global.datetimes.splice(index, 1)
    }

    global.latitudes.push(latitude)
    global.longitudes.push(longitude)
    global.emails.push(email)
    global.datetimes.push(dt)
});


router.get("/sign-up", (req, res) => {
    res.oidc.login({
        returnTo:'/profile',
    })
})

router.get("/profile", requiresAuth(), function(req, res){
    const user = req.oidc.user;

    req.user = {
        isAuthenticated : req.oidc.isAuthenticated(),
    };
    if (req.user.isAuthenticated){
        req.user.name = req.oidc.user.name;
        var email = req.oidc.user.email;

        req.user.email = email         
    }   
    
    req.user.emails = global.emails
    req.user.datetimes = global.datetimes
    req.user.latitudes= global.latitudes
    req.user.longitudes = global.longitudes

    res.render("profile", {user: req.user});
     
});

module.exports = router;