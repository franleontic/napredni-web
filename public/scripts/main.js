
var x = document.getElementById("location");

function getLocation() {
    console.log(x)
  if (navigator.geolocation) {
    navigator.geolocation.watchPosition(showPosition);
  } else {
    x.innerHTML = "Geolocation is not supported by this browser.";
  }
}
function showPosition(position) {
  x.innerHTML = "Latitude: " + position.coords.latitude +
  "<br>Longitude: " + position.coords.longitude;
}

function openStreetMapsLocate(){
  const status = document.querySelector("#status");
  const mapLink = document.querySelector("#map-link");

  mapLink.href = '';
  mapLink.textContent = '';

  function success(position) {
    const latitude = position.coords.latitude;
    const longitude = position.coords.longitude;

    const latminus = latitude - 0.01
    const latplus = latitude + 0.01
    const longminus = longitude - 0.01
    const longplus = longitude + 0.01
    
    status.textContent = '';


    var link = `https://www.openstreetmap.org/export/embed.html?bbox=${longminus}%2C${latminus}%2C${longplus}%2C${latplus}&layer=mapnik&marker=${latitude}%2C${longitude}`;

    var iframe = document.createElement('iframe');
    iframe.src = link;
    iframe.id = "mapiframe"
    iframe.width = "400";
    iframe.height = "400";

    var md = document.getElementById("mapdiv");
    md.appendChild(iframe);
  }

  function error() {
    status.textContent = 'Unable to retrieve your location';
  }

  if (document.querySelector("#mapiframe") != null){
    return;
  }

  if(!navigator.geolocation) {
    status.textContent = 'Geolocation is not supported by your browser';
  } else {
    status.textContent = 'Locating…';
    navigator.geolocation.getCurrentPosition(success, error);
  }
  
}
