var userParsed = JSON.parse(user)
var latitudes = userParsed.latitudes
var longitudes  = userParsed.longitudes
var emails = userParsed.emails
var datetimes = userParsed.datetimes

var email = userParsed.email



if (navigator.geolocation) {
  navigator.geolocation.getCurrentPosition(showPosition);
} else {
  console.log("Geolocation is not supported by this browser.");
}


function showPosition(position) {
  var latitude = position.coords.latitude;
  var longitude = position.coords.longitude;


  fetch("/locdata", {
  method: "post",
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
  },

  body: JSON.stringify({
    latitude: latitude,
    longitude: longitude,
    dt: "datetime",
    email: email
  })
})
.then( (response) => { 
  console.log("Posted")
});
}


var map = new ol.Map({
    target: 'map',
    layers: [
      new ol.layer.Tile({
        source: new ol.source.OSM()
      })
    ],
    view: new ol.View({
      center: ol.proj.fromLonLat([15.96, 45.81]),
      zoom: 8
    })
  });
    

if (latitudes != null){
  f = []
for (let i = 0; i < latitudes.length; i++) {
  var feature = new ol.Feature({
    geometry: new ol.geom.Point(ol.proj.fromLonLat([parseFloat(longitudes[i]), parseFloat(latitudes[i])]))
  })
  feature.name = "Last login: " + datetimes[i] + " " + emails[i];
  f.push(feature)
}

var layer = new ol.layer.Vector({
    source: new ol.source.Vector({
        features: f
    })
});
map.addLayer(layer);




const element = document.getElementById('popup');
const popup = new ol.Overlay({
  element: element,
  positioning: 'bottom-center',
  stopEvent: false,
});
map.addOverlay(popup);

map.on('click', function (evt) {
  const feature = map.forEachFeatureAtPixel(evt.pixel, function (feature) {
    return feature;
  });
  if (feature) {
    $(element).popover('dispose');
    popup.setPosition(evt.coordinate);
    $(element).popover({
      placement: 'top',
      html: false,
      content: feature.name,
    });
    $(element).popover('show');
  } else {
    $(element).popover('dispose');
  }
});


// Close the popup when the map is moved
map.on('movestart', function () {
  $(element).popover('dispose');
});
}



